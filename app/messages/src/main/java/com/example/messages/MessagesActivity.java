package com.example.messages;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MessagesActivity extends AppCompatActivity {

    Button buttonToast, buttonSnackBar, buttonDialog;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        buttonToast = findViewById(R.id.button_message_1);
        buttonSnackBar = findViewById(R.id.button_message_2);
        buttonDialog = findViewById(R.id.button_message_3);
        linearLayout = findViewById(R.id.linearlayout);

        buttonToast.setOnClickListener(v -> Toast.makeText(getApplicationContext(), R.string.msg_toast, Toast.LENGTH_LONG).show());

        buttonSnackBar.setOnClickListener(v -> Snackbar.make(linearLayout, R.string.msg_snack_bar, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_snack_bar_text, v1 -> {

        }).show());

        buttonDialog.setOnClickListener(v -> {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog
                    .setTitle(R.string.dialog_title)
                    .setMessage(R.string.dialog_text)
                    .setNegativeButton(R.string.no, (dialog, which) -> dialog.cancel())
                    .setPositiveButton(R.string.si, (dialog, which) -> {
                        //ඞඞ
                    }).show();
            alertDialog.create();
        });
    }
}