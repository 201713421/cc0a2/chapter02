package com.example.radiobutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class RadioButtonActivity extends AppCompatActivity {

    ImageView imageView;
    RadioGroup radioGroup;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_button);

        imageView = findViewById(R.id.image_view_logo);
        radioGroup = findViewById(R.id.radio_group);
        radioButton1 = findViewById(R.id.radio_button_logo_1);
        radioButton2 = findViewById(R.id.radio_button_logo_2);
        radioButton3 = findViewById(R.id.radio_button_logo_3);
        button = findViewById(R.id.button);

        button.setOnClickListener(v -> {
            if (radioButton1.isChecked()) {
                imageView.setImageResource(R.drawable.a);
                return;
            }
            if (radioButton2.isChecked()) {
                imageView.setImageResource(R.drawable.b);
                return;
            }
            if (radioButton3.isChecked()) {
                imageView.setImageResource(R.drawable.c);
            }
        });

    }
}