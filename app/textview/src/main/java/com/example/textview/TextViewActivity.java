package com.example.textview;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class TextViewActivity extends AppCompatActivity {

    TextView text_view_1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);

        text_view_1 = findViewById(R.id.text_view_1);

        text_view_1.setOnClickListener(v -> {
            text_view_1.setText(R.string.text_view_1_en);
            text_view_1.setBackgroundColor(Color.RED);
            text_view_1.setTextColor(Color.YELLOW);
        });
    }
}