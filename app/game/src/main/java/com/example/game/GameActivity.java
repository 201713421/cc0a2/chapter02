package com.example.game;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    TextView textViewLastAttempt, textViewRemainAttempt, textViewHint;
    EditText editTextGuessNumber;
    Button buttonFindGuessNumber;

    Boolean twoDigits, threeDigits, fourDigits;

    Random r = new Random();
    int random;

    int remainAttempts = 10;

    ArrayList<Integer> guessesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        textViewLastAttempt = findViewById(R.id.text_view_last_attempt);
        textViewRemainAttempt = findViewById(R.id.text_view_remain_attempts);
        textViewHint = findViewById(R.id.text_view_hint);
        editTextGuessNumber = findViewById(R.id.edit_text_guess_number);
        buttonFindGuessNumber = findViewById(R.id.button_find_guess_number);

        twoDigits = getIntent().getBooleanExtra("TWO", false);
        threeDigits = getIntent().getBooleanExtra("THREE", false);
        fourDigits = getIntent().getBooleanExtra("FOUR", false);

        if (twoDigits) {
            random = 10 + r.nextInt(90);
        }
        if (threeDigits) {
            random = 100 + r.nextInt(900);
        }
        if (fourDigits) {
            random = 1000 + r.nextInt(9000);
        }

        buttonFindGuessNumber.setOnClickListener(v -> {
            String sGuess = editTextGuessNumber.getText().toString();
            if (sGuess.equals("")){
                Toast.makeText(GameActivity.this, R.string.edit_text_msg, Toast.LENGTH_LONG).show();
                return;
            }

            remainAttempts--;

            int iGuess = Integer.parseInt(sGuess);

            guessesList.add(iGuess);

            textViewLastAttempt.setVisibility(View.VISIBLE);
            textViewRemainAttempt.setVisibility(View.VISIBLE);
            textViewHint.setVisibility(View.VISIBLE);

            Resources res = getResources();
            textViewLastAttempt.setText(String.format(res.getString(R.string.text_view_last_attempt), sGuess));
            textViewRemainAttempt.setText(String.format(res.getString(R.string.text_view_remain_attempt), iGuess));

            if (random == iGuess) {
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.game_msg_win), random, (10-remainAttempts), guessesList));
                builder.setPositiveButton("Si", (dialog, which) -> {
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.create().show();
            }
            if (random < iGuess) {
                textViewHint.setText(R.string.text_view_hint_decrease);
            }
            if (random > iGuess) {
                textViewHint.setText(R.string.text_view_hint_increase);
            }

            if (remainAttempts == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.game_msg_lose), random, guessesList));
                builder.setPositiveButton("Si", (dialog, which) -> {
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.create().show();
            }

            editTextGuessNumber.setText("");
        });
    }
}